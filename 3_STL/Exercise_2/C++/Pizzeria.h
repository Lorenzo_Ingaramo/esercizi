#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      int Price;
      string Description;
  };

  class Pizza {
    public:
      string Name;

      void AddIngredient(const Ingredient& ingredient) { throw runtime_error("Unimplemented method"); }
      int NumIngredients() const { throw runtime_error("Unimplemented method"); }
      int ComputePrice() const { throw runtime_error("Unimplemented method"); }
  };

  class Order {
    public:
      void InitializeOrder(int numPizzas) { throw runtime_error("Unimplemented method"); }
      void AddPizza(const Pizza& pizza) { throw runtime_error("Unimplemented method"); }
      const Pizza& GetPizza(const int& position) const { throw runtime_error("Unimplemented method"); }
      int NumPizzas() const { throw runtime_error("Unimplemented method"); }
      int ComputeTotal() const { throw runtime_error("Unimplemented method"); }
  };

  class Pizzeria {
    public:
      void AddIngredient(const string& name,
                         const string& description,
                         const int& price) { throw runtime_error("Unimplemented method"); }
      const Ingredient& FindIngredient(const string& name) const { throw runtime_error("Unimplemented method"); }
      void AddPizza(const string& name,
                    const vector<string>& ingredients) { throw runtime_error("Unimplemented method"); }
      const Pizza& FindPizza(const string& name) const { throw runtime_error("Unimplemented method"); }
      int CreateOrder(const vector<string>& pizzas) { throw runtime_error("Unimplemented method"); }
      const Order& FindOrder(const int& numOrder) const { throw runtime_error("Unimplemented method"); }
      string GetReceipt(const int& numOrder) const { throw runtime_error("Unimplemented method"); }
      string ListIngredients() const { throw runtime_error("Unimplemented method"); }
      string Menu() const { throw runtime_error("Unimplemented method"); }
  };
};

#endif // PIZZERIA_H
