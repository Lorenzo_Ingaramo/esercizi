#ifndef SHOPPING_H
#define SHOPPING_H

#include <iostream>
#include <vector>
#include <list>
#include <stack>
#include <queue>
#include <unordered_set>

using namespace std;

namespace ShoppingLibrary {

  class IShoppingApp {
    public:
      virtual unsigned int NumberElements() const = 0;
      virtual void AddElement(const string& product) = 0;
      virtual void Undo() = 0;
      virtual void Reset() = 0;
      virtual bool SearchElement(const string& product) = 0;
  };

  class VectorShoppingApp : public IShoppingApp {
    private:
      vector<string> elements;
      /// la classe vector gestisce dinamicamente i nostri array
    public:
    ///  void Temp(){
          /// elements.resize(10);
          /// elements[5]="pippo";  /// l'accesso ad un array ha complessità costante
          /// elements.reserve(10); /// in questo modo sto riservando il posto per almeno 10 elementi
          /// elements.push_back("pippo2");} /// aggiunta in coda, senza sapere la dimensione dell'array; tuttavia non sapendo quanto sia lungo l'array, devo scorrerlo tutto (come le liste)
      unsigned int NumberElements() const { return elements.size(); }
      void AddElement(const string& product) { elements.push_back(product); }
      void Undo() { elements.pop_back(); }
      void Reset() { elements.clear(); }
      bool SearchElement(const string& product)
      {
          /// ricerca più semplice del mondo
          unsigned int numElements = NumberElements();
          for (unsigned int i = 0; i < numElements; i++ )
          {
              if (elements[i]==product);
              return true;
          }
          return false;
      }
  };

  class ListShoppingApp : public IShoppingApp {
    private:
      list<string> elements;
    public:
      unsigned int NumberElements() const { return elements.size(); }
      void AddElement(const string& product) { elements.push_back(product); }
      void Undo() { elements.pop_back(); }
      void Reset() { elements.clear(); }
      bool SearchElement(const string& product)
      {
          for (list<string>::iterator it = elements.begin(); it != elements.end(); it++)
          {
              const string element = *it; /// sono sicuro di non poterla modificare
              if (element==product)
                  return true;
          }
          return false;
      }
  };

  class QueueShoppingApp : public IShoppingApp {
    private:
      queue<string> elements;
    public:
      unsigned int NumberElements() const { return elements.size(); }
      void AddElement(const string& product) { elements.push(product); }
      void Undo() {
          queue<string> queueNew;
          unsigned int numElements = NumberElements();
          for (unsigned int i = 0; i < numElements - 1, i++ )
          {
              queueNew.push(elements.back());
              elements.pop();
          }
          elements.pop();
          for (unsigned int i = 0; i < numElements)

      }
      void Reset() { cose varie }
      bool SearchElement(const string& product) {
          queue<string> cose varissimeeeee

      }
  };

  class StackShoppingApp : public IShoppingApp {
  private:
      stack<string> elements;
  public:
      unsigned int NumberElements() const { return elements.size(); }
      void AddElement(const string& product) { elements.push(product); }
      void Undo() { elements.pop(); }
      void Reset() {
          unsigned int numElements = NumberElements();
          for (unsigned int i = 0; i < numElements; i++)
              elements.pop();
          ; }
      bool SearchElement(const string& product) {
          stack<string> stackNew;
          bool found =
          unsigned int numElements = NumberElements();
          for (unsigned int i = 0; i < numElements, i++)
          {
              string& element = elements.top();
              if ( element == product)
              {
                  found = true;
                  break;
              }
          stackNew.push(element);
          elements.pop();
          }
      unsigned int numElementsNew = stackNew.size();
      for (unsigned int i = 0; i < numElementsNew, i++)
      {
          elements.push(stackNew.top());
          stackNew.top();
      }
      return found;
      }
  };

  class HashShoppingApp : public IShoppingApp {
      /// struttura dati avanzata senza un ordinamento preciso: esso è dettato da una funzione di indice:
      /// ogni elemento ha una chiave attraverso la quale si può ricercare
    private:
      unordered_set<string> elements;
      string lastElement;
    public:
      unsigned int NumberElements() const { return elements.size(); }
      void AddElement(const string& product) { elements.insert(product); }
      void Undo() { elements.erase(lastElement); }
      void Reset() { elements.clear(); }
      bool SearchElement(const string& product)
      {
          return elements.find(product) != elements.end();
      }
  };
}

#endif // SHOPPING_H
