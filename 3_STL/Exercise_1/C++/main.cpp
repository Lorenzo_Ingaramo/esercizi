#include "test_shopping.h"

#include <gtest/gtest.h>

int main(int argc, char *argv[])
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

/// solo per lanciare i test, altrimenti è inutile
