# Bus - MapViewer and RoutePlanner 

Buses of a city usually are accessible from a MapViewer application, such as `GoogleMap`.
Moreover, time travel computation of Bus routes is available in this application, and sometimes the cost estimation of the travel.

## Example 

The image below represent the map interface of the application with 4 different routes:

![bus](Images/map.png)

## Requirements

Write a software which reproduce a Bus Route Map and Planner which follows the following structure:

![bus_cd](Images/bus_cd.png)

Once the structure is created implements two different Bus Route Map provider, namely `GoogleMap` and `ViaMichelin`.

### GoogleMap

Requirements for `GoogleMap` are the following:

* Buses are loaded using the file `googleBuses.txt` of the following format:
    ```text
    # Number of buses
    3
    ```
    Bus id are numbered from `1` to the total number of buses.
    If something goes wrong the functions shall throw an exception `"Something goes wrong"`
* The map is filled using the file `googleMap.txt` of the following format:
  ```text
    # Number of busStop
    3
    # Id Name Lat Lon
    1 Cadorna 450781 76761
    2 Politecnico 450791 76771
    3 Castello 450771 76781
    # Number of Streets
    4
    # Id From To TravelTime(s)
    1 1 2 17
    2 2 3 48
    3 1 3 45
    4 3 1 24
    # Number of Routes
    2
    # Id NumberStreets StreetIds
    1 3 1 2 4
    2 2 3 4
    ```
    The element id are numbered from `1` to the total number of elements.

    If something goes wrong the functions shall throw an exception `"Something goes wrong"`
* MapViewer shows the elements with the following format:
    1. BusStop: `Name + " (" + Latitude + ", " + Longitude ")"`

        **Example**: `"Cadorna (45.078100, 7.676100)"`
    1. Street: `Id + ": " + From.Name + "->" + To.Name`
    
        **Example**: `"1: Cadorna -> Politecnico"`
    1. Route: `Id": + "` foreach street ` + From.Name + "->" +` last street ` + To.Name`
    
        **Example**: `"1: Cadorna -> Politecnico -> Castello -> Cadorna"`
    
    If the element (Route, BusStop or Street) does not exists, an exception shall be raised of message `"{elementName}" + id + " does not exists"`.

    If a street does not exists at position in route, an exception shall be raised of message `"Street at position" + streetPosition + " does not exists"`.

    **Example**: `"Route 17 does not exists"`
* RoutePlanner does not support the route cost computation, and return an exception `"No cost compuation available"` if requested.
    
    On the other hand, the RoutePlanner travel time computation for the route requested is performed as:
    ```C#
    foreach street in route totalTravelTime += (street.TravelTime * BusEnhancementStopTime)
    ```
    where `BusEnhancementStopTime` is an a-dimensional public static variable that can be set by the user.

    If the element (Route, BusStop or Street, Bus) does not exists, an exception shall be raised of message `"{elementName}" + id + " does not exists"`.

    **Example**: `"Bus 1 does not exists"`

### ViaMichelin

Requirements for `ViaMichelin` are the following:

* Buses are loaded using the file `viaMichelinBuses.txt` of the following format:
    ```text
    # Number of buses
    3
    # Buses
    # Line FuelCost(EUR/km)
    1 15
    2 23
    3 19
    ```
    Bus id are numbered from `1` to the total number of buses.
    If something goes wrong the functions shall throw an exception `"Something goes wrong"`
* The map is filled using the file `viaMichelinMap.txt` of the following format:
    ```text
    # Number of busStop
    3
    # Id Name Lat Lon
    1 Cadorna 450781 76761
    2 Politecnico 450791 76771
    3 Castello 450771 76781
    # Number of Streets
    4
    # Id From To TravelTime(s)
    1 1 2 17
    2 2 3 48
    3 1 3 45
    4 3 1 24
    # Number of Routes
    2
    # Id NumberStreets StreetIds
    1 3 1 2 4
    2 2 3 4
    ```
    The element id are numbered from `1` to the total number of elements.

    If something goes wrong the functions shall throw an exception `"Something goes wrong"`
* MapViewer shows the elements with the following format:
    1. BusStop: `Name + " (" + Latitude + ", " + Longitude ")"`

        **Example**: `"Cadorna (45.078100, 7.676100)"`
    1. Street: `Id + ": " + From.Name + "->" + To.Name`
    
        **Example**: `"1: Cadorna -> Politecnico"`
    1. Route: `Id": + "` foreach street ` + From.Name + "->" +` last street ` + To.Name`
    
        **Example**: `"1: Cadorna -> Politecnico -> Castello -> Cadorna"`
    
    If the element (Route, BusStop or Street) does not exists, an exception shall be raised of message `"{elementName}" + id + " does not exists"`.

    If a street does not exists at position in route, an exception shall be raised of message `"Street at position" + streetPosition + " does not exists"`.

    **Example**: `"Route 17 does not exists"`
* RoutePlanner route cost computation is performed as follow:
    ```C#
    foreach street in route totalTravelTime += street.TravelTime * bus.fuelCost * BusAverageSpeed 
    ```
    where `BusAverageSpeed` (km/h) is a public static variable that can be set by the user.
    
    The Route Planner TravelTime computation for the route requested is performed as:
    ```C#
    foreach street in route totalTravelTime += street.TravelTime
    ```
    If the element (Route, BusStop or Street, Bus) does not exists, an exception shall be raised of message `"{elementName}" + id + " does not exists"`.

    **Example**: `"Bus 1 does not exists"`*