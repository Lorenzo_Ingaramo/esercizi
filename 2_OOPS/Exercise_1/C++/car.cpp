#include "car.h"


namespace CarLibrary {

/// tre parametri passati dal main o dal test
Car::Car(const string &producer,
         const string &model,
         const string &color)
{
    _producer = producer;
    _color = color;
    _model = model;
}

string Car::Show()
{
    return _model + " (" + _producer +"(" + _color;
}

}
