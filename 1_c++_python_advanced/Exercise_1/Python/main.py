# in Python c'è la libreria NUMPY per l'algebra lineare; inoltre c'è SCIPY che amplia le funzionalità di NUMPY
import numpy as np
import scipy as sp
from scipy.linalg import lu_factor, lu_solve

def shape(n: int, m: int = 0):
    """
    shape   Shaped matrix.
        shape(n,m) is the n-by-m matrix with elements from 1 to n * m.
    """
    A = np.empty((n, m)) # non avviene alcuna inizializzazione
    # A = np.zeros((n, m)) # numpy vuole le doppie parentesi tonde!!
    # A[0,2]=0.1 significa andare a inserire 0.1 nel posto prima riga, terza colonna
    # A[2]=[2.5, 2, 6, 7] significa inserire i numeri nella terza riga
    # A[:, 1]=[1, 2, 3, 4] significa inserire quei numeri nella seconda colonna ( : indica tutte le righe)

    counter = 1.0
    for i in range (0, n):
        for j in range (0, m):
            A[i, j] = counter
            counter += 1

    print(A)

    return A


def rand(n: int, m: int = 0):
    """
    rand   Random matrix.
        rand(n,m) is the n-by-m matrix with random elements.
    """

    A=np.random.rand(n, m) # all'interno del package random, troviamo la matrice rand => che è quella che ci interessa
    print(A)

    return A


def hilb(n: int, m: int = 0):
    """
    hilb   Hilbert matrix.
       hilb(n,m) is the n-by-m matrix with elements 1/(i+j-1).
       it is a famous example of a badly conditioned matrix.
       cond(hilb(n)) grows like exp(3.5*n).
       hilb(n) is symmetric positive definite, totally positive, and a
       Hankel matrix.
    """

    A = np.zeros((n, m))

    for i in range (0, n):
        for j in range (0, m):
            A[i, j] = 1.0 / (i + j + 1.)
    print(A)

    return A


def solveSystem(A):
    """
    solveSystem   Solve system with matrix.
       solveSystem(A) solve the linear system Ax=b with x ones.
       returns the determinant, the condition number and the relative error
    """


    dim = A.shape[0]
    b = np.zeros((dim, 1)) # crea un vettore colonna
    b = A.sum(axis=1)[:, np.newaxis] # se axis è 1 somma le righe, se è 0 somma le colonne
    # con [:, np.newaxis] gli elementi del vettore riga vengono messi in colonna
    solution = np.ones((dim, 1))

    lu, piv = lu_factor(A)
    x = lu_solve((lu, piv), b) # risoluzione della matrice fattorizzata con PA=LU

    detA = np.linalg.det(A) # facile facile
    condA = np.linalg.cond(A) # facile facile
    errRelA = np.linalg.norm(solution - x) / np.linalg.norm(solution)

    return detA, condA, errRelA


if __name__ == '__main__':
    n: int = 4

    [detAS, condAS, errRelS] = solveSystem(shape(n, n))
    print("{:} - DetA: {:.4e}, RCondA: {:.4e}, Relative Error: {:.4e}".format('shape', detAS, 1. / condAS, errRelS))
    [detAR, condAR, errRelR] = solveSystem(rand(n, n))
    print("{:} - DetA: {:.4e}, RCondA: {:.4e}, Relative Error: {:.4e}".format('rand', detAR, 1. / condAR, errRelR))
    [detAH, condAH, errRelH] = solveSystem(hilb(n, n))
    print("{:} - DetA: {:.4e}, RCondA: {:.4e}, Relative Error: {:.4e}".format('hilb', detAH, 1. / condAH, errRelH))


    exit(0)
