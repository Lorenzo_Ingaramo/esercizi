#include <iostream>
#include "Eigen" /// le virgolette indicano di cercare la libreria nelle cartelle del progetto, dove appunto sono

using namespace std;
using namespace Eigen;


/// \brief shape(n,m) is the n-by-m matrix with elements from 1 to n * m.
/// il tipo MatrixXd è un tipo creato dalla libreria Eigen (d sta per double). Esistono anche le matrixXf (float) per esempio.
MatrixXd Shape(const int& n,
               const int& m = 0);
/// \brief rand(n,m) is the n-by-m matrix with random elements.
MatrixXd Rand(const int& n,
              const int& m = 0);
/// \brief hilb(n,m) is the n-by-m matrix with elements 1/(i+j-1).
MatrixXd Hilb(const int& n,
              const int& m = 0);
///
/// \brief solveSystem(A) solve the linear system Ax=b with x ones.
/// \param detA the determinant of A
/// \param condA the condition number of A
/// \param errRel the realtive error
void SolveSystem(const MatrixXd& A,
                 double& detA,
                 double& condA,
                 double& errRel);

int main()
{
  int n = 4;

  double detAS, condAS, errRelS;
  SolveSystem(Shape(n, n), detAS, condAS, errRelS);
  cout<< scientific<< "shape - DetA: "<< detAS<< ", RCondA: "<< 1.0 / condAS<< ", Relative Error: "<< errRelS<< endl;

  double detAR, condAR, errRelR;
  SolveSystem(Rand(n, n), detAR, condAR, errRelR);
  cout<< scientific<< "rand - DetA: "<< detAR<< ", RCondA: "<< 1.0 / condAR<< ", Relative Error: "<< errRelR<< endl;

  double detAH, condAH, errRelH;
  SolveSystem(Hilb(n, n), detAH, condAH, errRelH);
  cout<< scientific<< "hilb - DetA: "<< detAH<< ", RCondA: "<< 1.0 / condAH<< ", Relative Error: "<< errRelH<< endl;

  return 0;
}


MatrixXd Shape(const int& n, const int& m)
{
    MatrixXd A = MatrixXd::Zero(n,m); /// creazione matrice di dimensione nxm di zeri (funz. Zero)
    /// A(1, 2)=0.2; /// seconda riga, terza colonna
    /// A.row(0) << 1, 3, 4, 5; /// prima riga
    /// A.col(2) << 8.4, 7, 8, 45; ///terza colonna

    int counter = 1;
    for ( int i = 0; i < n; i++){
        for ( int j = 0; j < m; j++)
            A(i, j) = counter++;}

    return A;
}

MatrixXd Rand(const int& n, const int& m)
{

   MatrixXd A = MatrixXd::Random(n, m);

   return A;
}


MatrixXd Hilb(const int& n, const int& m)
{
    MatrixXd A = MatrixXd::Zero(n, m);

    for ( int i = 0; i < n; i++){
        for ( int j = 0; j < m; j++)
            A(i, j) = 1.0 / (i + j + 1.0);}

    return A;
}

void SolveSystem(const MatrixXd& A,
                 double& detA,
                 double& condA,
                 double& errRel)
{
  int dim = A.rows(); /// rows oppure cols (se voglio prendere righe o colonne)

  VectorXd b = VectorXd::Zero(dim);
  b = A.rowwise().sum(); /// ciascuna riga sommata va all'interno di b in questo modo: la somma della prima riga va in b[0] ecc...
  /// fattorizzazione PA=LU
  VectorXd x = A.fullPivLu().solve(b); /// b è il termine noto rispetto al quale chiediamo di risolvere

  detA = A.determinant();

  JacobiSVD <MatrixXd> svd(A); /// JacobiSVD calcola la decomp. ai val. singolari della matrice
  condA = svd.singularValues()[0]/svd.singularValues()[dim-1];

  VectorXd solution = VectorXd::Ones(dim);
  errRel = (solution - x).norm()/solution.norm(); /// è automatica la norma 2
}
