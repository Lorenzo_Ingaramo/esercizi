cmake_minimum_required(VERSION 3.5)

project(dotProduct LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(dotProduct main.cpp)
file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/vectors.txt
     DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
