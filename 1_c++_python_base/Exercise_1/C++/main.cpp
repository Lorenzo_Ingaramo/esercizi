#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

/// \brief ImportVectors reads the input vectors from file for dot product
/// \param filePath: path name of the input file
/// \param n: resulting size of the vectors
/// \param v1: resulting vector1
/// \param v2: resulting vector2
/// \return the result of the reading, true is success, false is error
bool ImportVectors(const string& inputFilePath,
                   size_t& n,
                   unsigned int*& v1,
                   unsigned int*& v2); /// per ogni funzione, in C++, qui c'è la dichiarazione, dopo il main ci sarà l'implementazione. Bisognerebbe sempre descrivere una funzione

/// \brief DotProduct performs the dot product between two vectors
/// \param n: size of the vectors
/// \param v1: the first vector
/// \param v2: the second vector
/// \param dotProduct: the resulting dot product
/// \return the result of the operation, true is success, false is error
bool DotProduct(const size_t& n,
                const unsigned int* v1,
                const unsigned int* v2,
                unsigned int& dotProduct);

/// \brief ExportResult export the result obtained in file
/// \param outputFilePath: path name of the output file
/// \param v1: vector1
/// \param v2: vector2
/// \param dotProduct: the dot product
/// \return the result of the export, true is success, false is error
bool ExportResult(const string& outputFilePath,
                  const size_t& n,
                  const unsigned int* v1,
                  const unsigned int* v2,
                  const unsigned int& dotProduct);


///// IGNORE THIS FUNCTION: questa funzione scrive i vettori nell'output del programma, ma è già stata fatta
string ArrayToString(const size_t& n,
                     const unsigned int* v)
{
  ostringstream toString; /// istringstream converte da stringa a numero, ostringstream fa esattamente il contrario
  toString << "[ ";
  for (unsigned int i = 0; i < n; i++)
    toString<< v[i]<< " ";
  toString << "]";

  return toString.str();
}

int main()
{
  string inputFileName = "./vectors.txt"; /// scrivere ./  significa comunicare al compilatore che il file è da andare a ricercare nella cartella del main
  size_t n = 0; ///il tipo size_t è un unsigned long
  unsigned int* v1 = nullptr; ///v1 e v2 sono PUNTATORI unsigned int
  unsigned int* v2 = nullptr;

  if (!ImportVectors(inputFileName, n, v1, v2)) ///ImportVectors è una funzione che bisogna poi implementare
  {
    cerr<< "Something goes wrong with import"<< endl; ///in caso di errore, restituisce -1 => essendo -1 negativo, indica la presenza di un errore
    return -1;
  }
  else
    cout<< "Import successful: n= "<< n<< " v1= "<< ArrayToString(n, v1)<< " v2= "<< ArrayToString(n, v2)<< endl; /// in caso di successo

  unsigned int dotProduct = 0;
  if (!DotProduct(n, v1, v2, dotProduct)) /// funzione per il prodotto scalare: avendo tutti numeri interi positivi => il prodotto scalare sarà positivo
  {
    cerr<< "Something goes wrong with dot product"<< endl; /// in caso di errore
    return -1;
  }
  else
    cout<< "Computation successful: result "<< dotProduct<< endl; /// in caso di successo

  string outputFileName = "./dotProduct.txt";
  if (!ExportResult(outputFileName, n, v1, v2, dotProduct))
  {
    cerr<< "Something goes wrong with export"<< endl;
    return -1;
  }
  else
    cout<< "Export successful"<< endl;

  delete[] v1; /// IMPORTANTE!!
  delete[] v2;

  return 0;
}

bool ImportVectors(const string& inputFilePath, /// il path è una stringa (costante): per evitare la copia, viene passata come referenza costante (infatti c'è la &)
                   size_t& n, /// da leggere dal file, idem per v1 e v2
                   unsigned int*& v1,
                   unsigned int*& v2)
{
    ifstream file; /// ifstream è un tipo di variabile, il nome è "file"
    file.open(inputFilePath);

    string line;
    getline(file, line); /// essendo una riga di commento, la sovrascrivo subito senza problemi
    getline(file, line);

    /// non potendo scrivere n=line, ho bisogno di un convertitore (lo prendiamo dalla libreria)
    istringstream convertN; /// qui creo il convertitore
    convertN.str(line); /// converte la stringa "e se la tiene", atraverso la funzione str
    convertN >> n; /// adesso il convertitore inserisce il valore nella stringa in un int (n) => sappiamo la dimensione dei vettori

    getline(file, line); /// essendo una riga di commento, la sovrascrivo subito senza problemi
    getline(file, line);

    v1 = new unsigned int[n]; /// nell'heap, ho creato n posizioni che conterranno unsigned int
    istringstream convertV1;
    convertV1.str(line); /// questo convertitore funziona come quello di prima, considerando però che scriviamo un ciclo for essendo in presenza di un vettore
    for (unsigned int i = 0; i < n; i++)
        convertV1 >> v1[i];

    getline(file, line); /// essendo una riga di commento, la sovrascrivo subito senza problemi
    getline(file, line);

    v2 = new unsigned int[n]; /// come per il vettore v1, uguale uguale
    istringstream convertV2;
    convertV2.str(line);
    for (unsigned int i = 0; i < n; i++)
        convertV2 >> v2[i];

    file.close();

    return true; }


bool DotProduct(const size_t& n, /// i tre input sono passati come referenza costante nel primo caso, come puntatore costante negli altri casi
                const unsigned int* v1,
                const unsigned int* v2,
                unsigned int& dotProduct) /// la variabile dotProduct viene passato come referenza, ma non costante in quanto dovrà essere scritto
{
    dotProduct = 0;
    for (unsigned int i = 0; i < n; i++)
        dotProduct += v1[i] * v2[i];

    return true; }


bool ExportResult(const string& outputFilePath,
                  const size_t& n,
                  const unsigned int* v1,
                  const unsigned int* v2,
                  const unsigned int& dotProduct) /// passando in input variabili modificabili, sono sicuro che queste vivranno anche dopo la fine della funzione
{
    ofstream file;
    file.open (outputFilePath);

    if (file.fail())
    {
        cerr << "Error on file open" << endl;
        return false;
    }

    file << "# Size of the two vectors" << endl; /// la funzione file scrive sul file (come cout sulla schermata di output)
    file << n << endl;

    file << "# vector 1" << endl;
    for (unsigned int i = 0; i < n; i++)
        file << ( i==0? "" : " ") << v1[i]; /// la parentasi sta per: se i=0 allora non mettere lo spazio, altrimenti mettilo
    file << endl;

    file << "# vector 2" << endl;
    for (unsigned int i = 0; i < n; i++)
        file << ( i==0? "" : " ") << v2[i];
    file << endl;

    file << "# dot product" << endl;
    file << dotProduct << endl;

    file.close();

    return true; }
