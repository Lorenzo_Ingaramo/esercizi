# \brief ImportVectors reads the input vectors from file for dot product
# \param filePath: path name of the input file
# \return the result of the reading, true is success, false is error
# \return n: resulting size of the vectors
# \return v1: resulting vector1
# \return v2: resulting vector2
def importVectors(inputFilePath): # è sempre bene scrivere una breve descrizione delle funzioni
    # ci sono diverse variabili in output e una sola in input (cosa che si può fare solo in Python)
    file = open(inputFilePath, 'r')

    lines = file.readlines() # le righe contengono il carattere "a capo"

    # n = lines[1] così n è una stringa => devo convertire
    n = int(lines[1]) #così va bene!
    # ATTENZIONE: senza conversione, nel ciclo for della terza funzione avremmo messo come range una stringa, cosa che
    #   non può funzionare

    # v1 = lines[3] di nuovo questa è una stringa => devo convertire
    v1 = [ int(s) for s in lines[3].split(' ')] # ZUCCHERO SINTATTICO

    v2 = [ int(s) for s in lines[5].split(' ')]

    file.close()

    return True, n, v1, v2

# volendo, si potrebbe inserire nelle funzioni le variabili con il tipo desiderato: ad esempio "inputFilePath:str"
#   indica che io sto passando una variabile di tipo stringa; anche perchè altrimenti Python decide il tipo in autonomia

# \brief DotProduct performs the dot product between two vectors
# \param n: size of the vectors
# \param v1: the first vector
# \param v2: the second vector
# \return the result of the operation, true is success, false is error
# \return dotProduct: the resulting dot product
def dotProduct(n, v1, v2):

    result = 0
    for i in range(0,n): # la n non è compresa => quindi abbiamo 0, 1, 2, 3, 4 => in tutto 5
        result += v1[i]*v2[i]

    return True, result

# \brief ExportResult export the result obtained in file
# \param outputFilePath: path name of the output file
# \param v1: vector1
# \param v2: vector2
# \param dotProduct: the dot product
# \return the result of the export, true is success, false
def exportResult(outputFilePath, n, v1, v2, dotProduct):

    file = open(outputFilePath, 'w')

    print("# Size of the two vectors", file=file)
    print(n, file=file)
    print("# vector 1", file=file)
    # print(v1, file=file) => scrivendo così però io ottengo [a, b, c, ...], ossia come vuole il default
    print(*v1, file=file) # * elimina parentesi quadre e virgole
    print("# vector 2", file=file)
    print(*v2, file=file)
    print("# dot product", file=file)
    print(dotProduct, file=file)

    file.close()

    return True


if __name__ == '__main__':
    inputFileName = "vectors.txt" # è stato omesso il ./ perchè non cambierebbe molto...
    # vediamo che in ogni passaggio c'è un controllo
    [resultImport, n, v1, v2] = importVectors(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1) # invece che return (in C++)
    else:
        print("Import successful: n=", n, " v1=", v1, " v2=", v2) # nel Python la scrittura a schermo dei vettori è decisamente più veloce e facile

    [resultDotProduct, dotProduct] = dotProduct(n, v1, v2)
    if not resultDotProduct:
        print("Something goes wrong with dot product")
        exit(-1)
    else:
        print("Computation successful: result ", dotProduct)

    outputFileName = "dotProduct.txt"
    if not exportResult(outputFileName, n, v1, v2, dotProduct):
        print("Something goes wrong with export")
        exit(-1)
    else:
        print("Export successful")
