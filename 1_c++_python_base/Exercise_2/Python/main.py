import sys

# \brief ImportText import the text for encryption
# \param inputFilePath: the input file path
# \return the result of the operation, true is success, false is error
# \return text: the resulting text
def importText(inputFilePath): # fatto, sembra funzionare

    file = open(inputFilePath, 'r')

    lines = file.readlines()
    string = lines

    file.close()

    return True, string

# \brief Encrypt encrypt the text
# \param text: the text to encrypt
# \param password: the password for encryption
# \return the result of the operation, true is success, false is error
# \return encryptedText: the resulting encrypted text
def encrypt(text, password):

    encText = text[0]
    n = len(encText) - 1
    p = len(password)
    molt: int = int(n/p)
    strPassword = password * (molt+1)

    for i in range (0, n):
       k = ord(encText[0])+ord(strPassword[0])
       if (k>127):
          k=k-127+31
       encText[i]=chr(k)
    print(encText)
    #encText[0] += chr(ord(encText[0])+ord(strPassword[0]))
    #print(strPassword[0], encText[0])


    return False, ""

# \brief Decrypt decrypt the text
# \param text: the text to decrypt
# \param password: the password for decryption
# \return the result of the operation, true is success, false is error
# \return decryptedText: the resulting decrypted text
def decrypt(text, password):
    return False, ""


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Password shall passed to the program")
        exit(-1)
    password = sys.argv[1]

    inputFileName = "text.txt"

    [resultImport, text] = importText(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: text=", text)

    [resultEncrypt, encryptedText] = encrypt(text, password)
    if not resultEncrypt:
        print("Something goes wrong with encryption")
        exit(-1)
    else:
        print("Encryption successful: result= ", encryptedText)

    [resultEncrypt, decryptedText] = decrypt(encryptedText, password)
    if not resultEncrypt or text != decryptedText:
        print("Something goes wrong with decryption")
        exit(-1)
    else:
        print("Decryption successful: result= ", decryptedText)
